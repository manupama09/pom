package utils;


import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel{

	public static Object[][] Createlead(String dataSheetName) throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook("./Data/"+dataSheetName+".xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);
		int lastRowNum = sheet.getLastRowNum();
		short lastCellNum = sheet.getRow(0).getLastCellNum();
		Object[][] data = new Object[lastRowNum][lastCellNum];
		for(int j=1;j<=lastRowNum;j++) {
			XSSFRow row=sheet.getRow(j);
			for(int i=0;i<lastCellNum;i++) {
				XSSFCell cell =row.getCell(i);
				try {
					String value = cell.getStringCellValue();
					data[j-1][i]=value;
				} catch (NullPointerException e) {
					System.out.println("");
				}
				
			}
			
			
		}
		wb.close();
		return data;
		
		
	}

}


