package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pomPages.LoginPage;
import wdMethods.ProjectMethod;

public class TC001_CreateLead extends ProjectMethod {
	
	@BeforeTest(groups= {"any"})
	public void setData()
	{
		testCaseName="Create Lead";
		testDesc="Creating a New Lead";
		category = "Smoke";
		author = "Anupama";
		dataSheetName = "TC001";
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLead1(String uName,String password,String companyName, String firstName, String lastName, String emailId, String contact)
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.enterEmailId(emailId)
		.enterContactNumber(contact)
		.clickSubmit();
	}
	
	

}
